package main

import (
	"encoding/json"
	"fmt"
	"github.com/cloudflare/cloudflare-go"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"
)

var (
	Config = AppConfig{}
	api    *cloudflare.API
)

type AppConfig struct {
	Interval    time.Duration `json:"interval"`
	Credentials struct {
		ApiKey string `json:"api_key"`
		Email  string `json:"email"`
	} `json:"credentials"`
	DNSZones []DNSZone `json:"dns_zones"`
}

type DNSZone struct {
	ZoneID   string
	ZoneName string      `json:"zone"`
	Records  []DNSRecord `json:"records"`
}

type DNSRecord struct {
	Name      string `json:"name"`
	CurrentIP string
	ID        string
	Proxied   bool
}

func main() {
	if _, err := os.Stat("./config.json"); os.IsNotExist(err) {
		log.Fatal("Could not find ./config.json")
	}
	if file, err := ioutil.ReadFile("./config.json"); err == nil {
		if err := json.Unmarshal([]byte(file), &Config); err != nil {
			log.Fatal("Could not parse ./config.json", err)
		}
	} else {
		log.Fatal("Could not read ./config.json", err)
	}

	log.Println("Initializing... ")
	currentIP := getPublicIP()
	api, _ = cloudflare.New(Config.Credentials.ApiKey, Config.Credentials.Email)
	for index, _ := range Config.DNSZones {
		if zoneID, err := api.ZoneIDByName(Config.DNSZones[index].ZoneName); err == nil {
			Config.DNSZones[index].ZoneID = zoneID
			records, _ := api.DNSRecords(zoneID, cloudflare.DNSRecord{})
			for _, rr := range records {
				for rid, _ := range Config.DNSZones[index].Records {
					if fmt.Sprintf("%s.%s", Config.DNSZones[index].Records[rid].Name, Config.DNSZones[index].ZoneName) == rr.Name {
						Config.DNSZones[index].Records[rid].ID = rr.ID
						Config.DNSZones[index].Records[rid].CurrentIP = rr.Content
						Config.DNSZones[index].Records[rid].Proxied = rr.Proxied
						if rr.Content != currentIP {
							Config.updateRecord(&Config.DNSZones[index], &Config.DNSZones[index].Records[rid], currentIP)
						}
					}
				}
			}
		} else {
			log.Fatal(err)
		}
	}
	log.Printf("Done, current ip is %s\n", currentIP)

	log.Println("Looking for ip changes...")
	ticker := time.NewTicker(Config.Interval * time.Second)
	for _ = range ticker.C {
		currentIP = getPublicIP()
		for index, _ := range Config.DNSZones {
			for rid, _ := range Config.DNSZones[index].Records {
				if Config.DNSZones[index].Records[rid].CurrentIP != currentIP {
					Config.updateRecord(&Config.DNSZones[index], &Config.DNSZones[index].Records[rid], currentIP)
				}
			}
		}
	}
}

func (config *AppConfig) updateRecord(zone *DNSZone, record *DNSRecord, ip string) {
	cfRecord, err := api.DNSRecord(zone.ZoneID, record.ID)
	if err != nil {
		log.Fatal(err)
	}
	cfRecord.Content = ip
	if err := api.UpdateDNSRecord(zone.ZoneID, cfRecord.ID, cfRecord); err != nil {
		log.Fatal(err)
	}
	record.CurrentIP = ip
	log.Printf("Set DNS %s record to ip %s\n", record.Name, cfRecord.Content)
}

func (config *AppConfig) getZone(name string) *DNSZone {
	for index, _ := range config.DNSZones {
		if config.DNSZones[index].ZoneName == name {
			return &config.DNSZones[index]
		}
	}
	log.Fatal("Requested invalid DNS zone")
	return &DNSZone{}
}

func getPublicIP() string {
	if res, err := http.Get("https://api.ipify.org"); err == nil {
		if ip, err := ioutil.ReadAll(res.Body); err == nil {
			return string(ip)
		} else {
			log.Fatal(err)
		}
	} else {
		log.Fatal(err)
	}
	return ""
}
